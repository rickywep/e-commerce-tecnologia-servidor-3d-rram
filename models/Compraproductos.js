const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ComprasproductosSchema = Schema({
  proveedorid: {
    type: Schema.Types.ObjectId,
    ref: 'Proveedor',
    required: true,
  },
  productoid: {
    type: Schema.Types.ObjectId,
    ref: 'Producto',
    required: true,
  },
  cantidad: {
    type: Number,
    trim: true,
    required: true
  },
  preciocompra: {
    type: Number,
    required: true
  },
  cambiaprecio: {
    type: Boolean,
    default: false,
  },
  precioventa: {
    type: Number,
    required: true
  },
  precioanterior: {
    type: Number,
    required: true
  },
  usuario: {
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  updated_at: {
    type: Date,
    default: Date.now()
  }
});
//esto es para update_at
ComprasproductosSchema.pre('save', async function (){
    this.updated_at = new Date();
});

module.exports = mongoose.model("Comprasproductos", ComprasproductosSchema);
