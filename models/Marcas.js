const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

//defino schema modelo
const MarcasSchema = new Schema({
    marca: {
        type: String,
        required: true,
        trim: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});
//esto es para update_at
MarcasSchema.pre('save', async function (){
    console.log(this);
    this.updated_at = new Date();
});
//pagination
MarcasSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Marcas', MarcasSchema);