const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const turnoController = require('../controllers/turnoController')
const auth = require('../middleware/auth');

router.post("/turnos", [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'Agrega un email valido').isEmail(),
    check('celular', 'Agrega un celular valido').isNumeric(),
    check('diaYHora', 'Debes elegir un dia y horario').notEmpty(),
    check('dia', 'Debes elegir un dia').notEmpty(),
], turnoController.crearTurno);

router.get("/turnos/:dia", 
turnoController.listarTurnos);

router.get("/turnos", 
turnoController.obtenerTurnos);

module.exports = router;
