const Proveedor = require('../models/Proveedor');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose')

//creo el proveedor
exports.crear = async (req, res) => {
    //validaciones
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array()});
    }
    try {
        // creo el proveedor
        let nuevoproveedor = new Proveedor(req.body);
        nuevoproveedor.usuario = req.usuario.id;
        const {nombre} = nuevoproveedor;
        // valido que no exista en la base
        let buscaproveedor = await Proveedor.findOne({ nombre });
        if(buscaproveedor){
            return res.status(400).json({ msg: 'El Proveedor ingresado ya Existe.'});
        } 
        //guardo en base mongoose
        await nuevoproveedor.save();
        res.json({ msg: 'Proveedor creado correctamente.', nuevoproveedor});
    } catch (error) {
        console.error(error);
        res.status(400).json({msg:'Hubo un error.'})
    }
};
// Actualizar Proveedor
exports.actualizar = async (req, res) => {
    
    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
 
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La Proveedor no existe en Mongoose.'});
        }
        // Verificar que proveedor exista
        let proveedor = await Proveedor.findById(req.params.id);

        if(!proveedor){
            return res.status(404).json({ msg: 'El Proveedor no existe.'});
        }
        //valido el modelo para que no existan 2 en mongoose
        //todos los proveedor mongoose
        let validoproveedor = await Proveedor.find();
        //inicializo el proveedor a modificar
        let updateproveedor = await Proveedor.findById(req.params.id);
        const {nombre} = updateproveedor;
        //filtro el proveedor que estoy por modificar de todos los q fig en mongoose
        proveedor = validoproveedor.filter(p => p._id != req.params.id);
        //al resultado valido que no exista el nombre para no duplicar
        let encontrado = proveedor.find(p => p.nombre === req.body.nombre)
        
        if(encontrado){
            return res.status(404).json({ msg: 'El Proveedor ya existe.'});
        }

        // Modificar proveedor... nota el tercer parametro es para q me devuelva la actualizacion del proveedor actualizado
        // Si no lo pongo me devuelve el proveedor antes de la modificacion 
        // marca = await Proveedor.findByIdAndUpdate(req.params.id, req.body, { new: true });

        //esto es para ejecutar el update de la fecha en el modelo del proveedor
         if(req.body.nombre){
            updateproveedor.nombre = req.body.nombre;
            updateproveedor.email = req.body.email;
            updateproveedor.celular = req.body.celular;
            updateproveedor.observaciones = req.body.observaciones;
        }
        await updateproveedor.save();
        
        res.json({ msg: 'El Proveedor se actualizo correctamente.', updateproveedor });
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
}
// Eliminar proveedor
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'El Proveedor no existe.'});
        }
 
        // Verificar que el proveedor exista
        let proveedor = await Proveedor.findById(req.params.id);
        
        if(!proveedor){
            return res.status(404).json({ msg: 'El Proveedor no existe.'});
        }
        //Falta validar que no tengas compras
 
        // Eliminar
        await proveedor.remove();        
        res.json({msg: 'Proveedor eliminado correctamente.'});
 
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
// Listar Todos los Proveedores
exports.listar = async (req, res) => {
    try {
        const proveedores = await Proveedor.find().sort('nombre');
        res.json({ proveedores });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Proveedores.' });
    }
}