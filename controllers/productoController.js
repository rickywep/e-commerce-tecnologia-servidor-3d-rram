const Productos = require('../models/Productos');
const Marcas = require('../models/Marcas');
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

// Crear producto
exports.crear = async (req, res) => {

    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        let nuevoproducto = new Productos(req.body);
        const {marcaid, modelo} = nuevoproducto;
        // Controlar la marca
        const marca = await Marcas.findById(marcaid);
        if(!marca){
            res.status(400).json({ msg: 'La Marca no existe.'});
        }
        // valido que no exista el modelo en la base
        let busproducto = await Productos.findOne({ modelo });
        if(busproducto){
            return res.status(400).json({ msg: 'El modelo ingresado ya Existe.'});
        }
        // Guardamos el producto en la BD
        await nuevoproducto.save();
        //populate
        nuevoproducto.marcaid = marca;
        res.json({ msg: 'Producto creado correctamente', nuevoproducto });
        
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error'});
    }
};
// Actualizar Productos
exports.actualizar = async (req, res) => {
    
    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
 
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La Producto no existe Mongoose.'});
        }
        let productoaux = new Productos(req.body);
        const {marcaid} = productoaux;
        // Controlar la marca
        const marca = await Marcas.findById(marcaid);
        if(!marca){
            res.status(400).json({ msg: 'La Marca no existe.'});
        }
        // Verificar que producto exista
        let producto = await Productos.findById(req.params.id);

        if(!producto){
            return res.status(404).json({ msg: 'El Producto no existe.'});
        }
        //valido el modelo para que no existan 2 en mongoose
        //todos los productos mongoose
        let validoproducto = await Productos.find();
        //inicializo el producto a modificar
        let updateproducto = await Productos.findById(req.params.id);
        const {modelo} = updateproducto;
        //filtro el producto que estoy por modificar de todos los q fig en mongoose
        producto = validoproducto.filter(p => p._id != req.params.id);
        //al resultado valido que no exista el modelo para no duplicar
        let encontrado = producto.find(p => p.modelo === req.body.modelo)
        
        if(encontrado){
            return res.status(404).json({ msg: 'El Producto ya existe.'});
        }

        // Modificar marca... nota el tercer parametro es para q me devuelva la actualizacion de la marca actualizado
        // Si no lo pongo me devuelve la marca antes de la modificacion 
        // marca = await Marcas.findByIdAndUpdate(req.params.id, req.body, { new: true });

        //esto es para ejecutar el update de la fecha en el modelo de la marca
         if(req.body.modelo){
            updateproducto.estadoCel = req.body.estadoCel;
            updateproducto.marcaid = req.body.marcaid;
            updateproducto.modelo = req.body.modelo;
            updateproducto.precio = req.body.precio;
            updateproducto.disponible = req.body.disponible;
            updateproducto.caracteristicas = req.body.caracteristicas;
            updateproducto.imgCel = req.body.imgCel;
        }
        await updateproducto.save();
        //populate
        updateproducto.marcaid = marca;

        //res.json({ msg: 'Producto se actualizo correctamente.', updateproducto });
        res.json({ msg: 'Producto se actualizo correctamente.', updateproducto });
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
}
// Eliminar productos
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'El Producto no existe.'});
        }
 
        // Verificar que el producto exista
        let producto = await Productos.findById(req.params.id);
        
        if(!producto){
            return res.status(404).json({ msg: 'El Producto no existe.'});
        }
 
        // Verificar que la producto le pertenezca al usuario autenticado
/*         if(producto.usuario.toString() != req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        } */
 
        // Eliminar
        await producto.remove();        
        res.json({msg: 'Producto eliminado correctamente.'});
 
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
// Listar Todos Productos
exports.listar = async (req, res) => {
    try {
        //paginado
        const { pagina, porpagina } = req.query;
        const options = {
            populate : 'marcaid',
            page : parseInt(pagina,10),
            limit : parseInt(porpagina,10),
        }

        const productos = await Productos.paginate({},options);
        //const productos = await Productos.filter(p => p != null).sort('marcaid');
        res.json({ productos });

        //const productos = await Productos.find().sort('marcaid').populate('marcaid');
        //const productos = await Productos.paginate();


    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Productos.' });
    }
}
// Listar Todos Productos por menor Precio
exports.listarMenorPrecio = async (req, res) => {
    try {
        const productos = await Productos.find().sort('precio')
        res.json({ productos });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Productos.' });
    }
}
// Listar Todos Productos por mayor Precio
exports.listarMayorPrecio = async (req, res) => {
    try {
        const productos = await Productos.find()
        productos.sort((a, b) => parseFloat(b.precio) - parseFloat(a.precio))
        res.json({ productos });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Productos.' });
    }
}
// Listar por marca
exports.listarProductoPorMarca = async (req, res) => {
    try {
        const marcaid = req.params.marcaid;
        const productos = await Productos.find({marcaid: marcaid})
        res.json({ productos });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Productos.' });
    }
}