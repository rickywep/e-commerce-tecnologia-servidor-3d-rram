const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Definir Schema

const CompraSchema = new Schema({
    fecha: {
        type: Date,
        default: Date.now()
    },
    nombre: {
        type: String,
        trim: true,
        required: true
    },
    apellido: {
        type: String,
        trim: true,
        required: true
    },
    domicilio: {
        type: String,
        trim: true,
        required: true
    },
    provincia: {
        type: String,
        trim: true,
        required: true
    },
    ciudad: {
        type: String,
        trim: true,
        required: true
    },
    productosComprados: {
        type: Array,
        required: true,
    },
    total: {
        type: Number,
        trim: true,
        required: true
    },
    codPostal: {
        type: Number,
        trim: true,
        required: true
    },
    telefono: {
        type: Number,
        trim: true,
        required: true
    },
    tipoEnvio: {
        type: String,
        trim: true,
        required: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario'
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
})


// Definimos el modelo con el Schema
module.exports = mongoose.model('Compra', CompraSchema);