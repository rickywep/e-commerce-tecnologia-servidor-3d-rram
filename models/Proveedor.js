const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProveedorSchema = Schema({
  nombre: {
    type: String,
    require: true,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    unique: true,
    trim: true,    
  },
  celular: {
    type: Number,
    trim: true,
    required: true
  },
  observaciones: {
    type: String,
    trim: true
  },
  usuario: {
    type: Schema.Types.ObjectId,
    ref: 'Usuario'
  },
  created_at: {
    type: Date,
    default: Date.now()
  },
  updated_at: {
    type: Date,
    default: Date.now()
  }
});
//esto es para update_at
ProveedorSchema.pre('save', async function (){
    console.log(this);
    this.updated_at = new Date();
});

module.exports = mongoose.model("Proveedor", ProveedorSchema);
