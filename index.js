const express = require('express');
const conectarDb = require('./config/db');
const cors = require ('cors')

// crear el servidor
const app = express();

// conectar BD
conectarDb();

//habilitando cors
app.use(cors())
//habilitar express.json
app.use(express.json({extend: true}))
app.use(express.urlencoded({ extended: true})); // for parsing application/x-www-form-urlencoded

//puertos
const PORT = process.env.PORT || 3000;

//importar rutas
app.use('/api', require('./routes/usuarios'))
app.use('/api', require('./routes/auth'))
app.use('/api', require('./routes/productosmarcas'))
app.use('/api', require('./routes/turnos'))
app.use('/api', require('./routes/mensaje'))
app.use('/api', require('./routes/proveedorstock'))
app.use('/api', require('./routes/compras'))

//arrancar servidor
app.listen(PORT, ()=>{
    console.log(`Servidor funcionando en el puerto ${PORT}`);
    
})