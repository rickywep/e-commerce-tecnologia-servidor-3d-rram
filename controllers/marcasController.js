const Marcas = require('../models/Marcas');
const Productos = require('../models/Productos')
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

//creo la marcas
exports.crear = async (req, res) => {
    //validaciones
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array()});
    }
    try {
        // creo la marcas
        let nuevamarca = new Marcas(req.body);
        nuevamarca.usuario = req.usuario.id;
        const {marca} = nuevamarca;
        // valido que no exista en la base
        let busmarca = await Marcas.findOne({ marca });
        if(busmarca){
            return res.status(400).json({ msg: 'La Marca ingresada ya Existe.'});
        } 
        //guardo en base mongoose
        await nuevamarca.save();
        res.json({ msg: 'Marca fue creada correctamente.', nuevamarca});
    } catch (error) {
        console.error(error);
        res.status(400).json({msg:'Hubo un error.'})
    }
};
// Actualizar Marcas
exports.actualizar = async (req, res) => {
    
    // Validación de campos
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
 
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La Marca no existe Mongoose.'});
        }
        // Verificar que la marca exista
        let marca = await Marcas.findById(req.params.id);

        if(!marca){
            return res.status(404).json({ msg: 'la marca no existe.'});
        }
        // Verificar que la marca pertenezca al usuario
/*         if(marca.usuario.toString() !== req.usuario.id){
            res.status(401).json({ msg: 'No autorizado.' });
        } */

        // Modificar marca... nota el tercer parametro es para q me devuelva la actualizacion de la marca actualizado
        // Si no lo pongo me devuelve la marca antes de la modificacion 
        // marca = await Marcas.findByIdAndUpdate(req.params.id, req.body, { new: true });
                
        //valido la marca para que no existan 2 en mongoose
        //todas las marcas de mongoose
        let validomarca = await Marcas.find();
        //filtro la marca que estoy por modificar de todos los q fig en mongoose
        let busmarca = validomarca.filter(p => p._id != req.params.id);
        //al resultado valido que no exista otra marca para no duplicar
        let encontrado = busmarca.find(p => p.marca === req.body.marca)
        
        if(encontrado){
            return res.status(404).json({ msg: 'La Marca ya existe.'});
        }

        //esto es para ejecutar el update de la fecha en el modelo de la marca
        if(req.body.marca){
            marca.marca = req.body.marca;
        }
        await marca.save();
        
        res.json({ msg: 'Marca actualizada correctamente.', marca });
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
}
// Eliminar Marcas
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La Marca no existe.'});
        }
 
        // Verificar que la Marca exista
        let marca = await Marcas.findById(req.params.id);
        
        if(!marca){
            return res.status(404).json({ msg: 'La Marca no existe.'});
        }
 
        // Verificar que la marca le pertenezca al usuario autenticado
/*         if(marca.usuario.toString() != req.usuario.id){
            return res.status(401).json({ msg: 'No autorizado.' });
        } */
        //solo se elimina si no existen productos
        const productosbus = await Productos.find({marcaid: req.params.id});
        if(productosbus.length > 0){
            return res.status(404).json({ msg: 'No puede Eliminar la Marca, Existen Productos.'});
        }
 
        // Eliminar
        await marca.remove();        
        res.json({msg: 'Marca eliminada correctamente.'});
 
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
// Listar Marcas
exports.listar = async (req, res) => {
    try {
        //const marcas = await Marcas.find({ usuario: req.usuario.id }).sort('marca');
        const { pagina, porpagina } = req.query;
        const options = {
            populate : 'usuario',
            page : parseInt(pagina,10),
            limit : parseInt(porpagina,10),
        }
        //const marcas = await Marcas.find().sort('marca').populate("usuario");
        const marcas = await Marcas.paginate({},options);
        res.json({ marcas });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de Marcas.' });
    }
}