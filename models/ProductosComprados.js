const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Definir Schema
const ProductosCompradosSchema = new Schema({
    cantidad: {
        type: Number,
        default: 0
    },
    productoid: {
        type: Schema.Types.ObjectId,
        ref: 'Producto',
        required: true,
    },
});

// Definimos el modelo con el Schema
module.exports = mongoose.model('ProductosComprados', ProductosCompradosSchema);