const Compraproductos = require('../models/Compraproductos');
const Productos = require('../models/Productos')
const { validationResult } = require('express-validator');
const mongoose = require('mongoose')

//creo la compra
exports.crear = async (req, res) => {
    //validaciones
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array()});
    }
    try {
        // creo la compra
        let nuevacompra = new Compraproductos(req.body);
        nuevacompra.usuario = req.usuario.id;
        const {cantidad, cambiaprecio, productoid} = nuevacompra;
        // Controlar que el producto exista
        const producto = await Productos.findById(productoid);
        if(!producto){
            res.status(400).json({ msg: 'El producto no existe.'});
        }
        //guardo en base mongoose
        await nuevacompra.save();
        //actualizo stock en coleccion productos y precio si corresponde
        producto.stock = producto.stock + nuevacompra.cantidad;
        if(cambiaprecio){
            producto.precio = nuevacompra.precioventa
        }
        await producto.save()

        res.json({ msg: 'Compra creada correctamente.', nuevacompra});
    } catch (error) {
        console.error(error);
        res.status(400).json({msg:'Hubo un error.'})
    }
};
// Eliminar compras y deshacer el proceso de act de stock
exports.eliminar = async (req, res) => {
    try {
        // Validar ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'La compra no existe.'});
        } 
        // Verificar que el compra exista
        let compra = await Compraproductos.findById(req.params.id);        
        if(!compra){
            return res.status(404).json({ msg: 'La compra no existe.'});
        }
        const {cantidad, cambiaprecio, productoid} = compra;
        // Controlar que el producto exista
        const producto = await Productos.findById(productoid);
        if(!producto){
            res.status(400).json({ msg: 'El producto no existe.'});
        } 
        // deshacer act de stock en productos y precios
        producto.stock = producto.stock - compra.cantidad;
        if(cambiaprecio){
            producto.precio = compra.precioanterior
        }
        await producto.save()
 
        // Eliminar compra
        await compra.remove();        
        res.json({msg: 'La compra fue eliminada correctamente.'});
 
    } catch (error) {
        console.log(error);
        res.status(400).json({ msg: 'Hubo un error.'});
    }
};
// Listar Todas las compras
exports.listar = async (req, res) => {
    try {
        const compras = await Compraproductos.find().sort('proveedorid').populate('proveedorid').populate('productoid');
        res.json({ compras });
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de compras de Productos.' });
    }
}

// Listar compras por proveedor y fechas
exports.listarProvFechas = async (req, res) => {
    try {
        let diainicial = req.header('diainicial');
        let diafinal = req.header('diafinal');
        let proveedor = req.header('proveedorid');

        //Example.find({ validUntil: { '$gte': start, '$lte': end })
        if (proveedor === "" || proveedor === "Todos..."){
            const compras = await Compraproductos.find(
                {created_at: { '$gte': diainicial, '$lte': diafinal} }).sort('proveedorid').
                populate('proveedorid', 'nombre').
                populate({ path: 'productoid', populate: { path: 'marcaid', select: 'marca' },select: 'modelo marcaid'});
            res.json({ compras });
        } else{
            const compras = await Compraproductos.find({proveedorid: proveedor, created_at: { '$gte': diainicial, '$lte': diafinal} }).sort('proveedorid').
                populate('proveedorid', 'nombre').
                populate({ path: 'productoid', populate: { path: 'marcaid', select: 'marca' },select: 'modelo marcaid'});
            res.json({ compras });
        }
    } catch (error) {
        console.log(error);
        return res.status(400).json({ msg: 'Hubo un error en el Servidor, Listado de compras de Productos.' });
    }
}
