const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Definir Schema

const TurnoSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true
    },
    celular: {
        type: Number,
        trim: true,
        required: true
    },
    diaYHora: {
        type: Date,
        trim: true,
        required: true
    },
    dia: {
        type: String,
        trim: true,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
})


// Definimos el modelo con el Schema
module.exports = mongoose.model('Turno', TurnoSchema);