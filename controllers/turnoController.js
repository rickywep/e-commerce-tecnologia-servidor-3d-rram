const Turno = require("../models/Turno");
const { validationResult } = require("express-validator");

exports.crearTurno = async (req, res) => {
  //Validacion de campos
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(402).json({ errors: errors.array() });
  }
  try {
    let turno = new Turno(req.body);

    // Guardamos el turno en la BD
    await turno.save();
    res.json({ msg: "Turno creado correctamente " + turno });
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: "Hubo un error" });
  }
};
exports.listarTurnos = async (req, res) => {
  try {
    const dia = req.params.dia;
    const turnos = await Turno.find({ dia: dia });
    res.json({ turnos });
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: "Hubo un error" });
  }
};

//obtener todos los turnos
exports.obtenerTurnos = async (req,res) =>{

    try {
       await Turno.find().select('-created_at').then(turno => res.send(turno))
      
    } catch (error) {
      console.log(error);
      res.status(500).send("hubo un error")    
    }
  }