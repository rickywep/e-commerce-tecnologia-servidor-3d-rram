const Compra = require('../models/Compra');
const Productos = require('../models/Productos')
const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

//creo la compra
exports.crearCompra = async (req, res) => {
  //validaciones
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try {
    // creo la compra
    let nuevacompra = new Compra(req.body);
    //guardo en base mongoose
    await nuevacompra.save();
    res.json({ msg: 'Compra fue creada correctamente.', nuevacompra });
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: 'Hubo un error.' })
  }
};

exports.obtenerCompras = async (req, res) => {

  try {
    await Compra.find().then(compras => res.send(compras))

  } catch (error) {
    console.log(error);
    res.status(500).send("hubo un error")
  }
}

exports.listarCompra = async (req, res) => {
  try {
    const user = req.params.user;
    const compras = await Compra.find({ user: user });
    res.json({ compras });
  } catch (error) {
    console.error(error);
    res.status(400).json({ msg: "Hubo un error" });
  }
};
exports.actualizarStock = async (req, res) => {

  // Validación de campos
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try {

    let { cantidad } = req.body
    const producto = await Productos.findById(req.params.id);
    producto.stock = producto.stock - cantidad;
    await producto.save()


    if (!producto) {
      return res.status(404).json({ msg: 'El producto no existe.' });
    }
    res.json({ msg: 'Stock restado.'});

  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: 'Hubo un error.' });
  }
}
