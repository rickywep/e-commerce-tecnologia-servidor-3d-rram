// Rutas para usuarios
const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const comprasController = require("../controllers/comprasController");
const auth = require('../middleware/auth');

router.post("/compras", [
    check('nombre', 'El nombre del cliente no debe estar vacio').notEmpty(),
    check('apellido', 'El nombre del apellido no debe estar vacio').notEmpty(),
    check('domicilio', 'El nombre del domicilio no debe estar vacio').notEmpty(),
    check('ciudad', 'El nombre del ciudad no debe estar vacio').notEmpty(),
    check('productosComprados', 'La compra debe contener algun producto').notEmpty(),
    check('total', 'El total no puede estar vacio').notEmpty(),
    check('total', 'El total debe ser numerico').isNumeric(),
    check('codPostal', 'El codPostal no puede estar vacio').notEmpty(),
    check('codPostal', 'El codPostal debe ser numerico').isNumeric(),
    check('telefono', 'El telefono no puede estar vacio').notEmpty(),
    check('telefono', 'El telefono debe ser numerico').isNumeric(),
    check('provincia', 'La provincia no puede estar vacia').notEmpty(),
    check('tipoEnvio', 'Falta el tipo de envio').notEmpty()
], comprasController.crearCompra);
router.get('/compras',
    auth,
    comprasController.obtenerCompras
)
router.get("/compras/:user", 
comprasController.listarCompra);
router.put('/compras/:id',
    auth,
    comprasController.actualizarStock
);


module.exports = router;
