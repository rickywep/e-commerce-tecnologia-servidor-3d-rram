const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const marcasController = require('../controllers/marcasController')
const productoController = require('../controllers/productoController')
const auth = require('../middleware/auth');

//creo la marcas
// api/marcas/
router.post('/marcas/',
    auth,
    [
        check('marca','El nombre de la Marca es Obligatorio.').notEmpty()
    ],
    marcasController.crear
);
// Actualizar marcas
// api/marcas/:id
router.put('/marcas/:id',
    auth,
    [
        check('marca','El nombre de la Marca es obligatorio.').notEmpty()
    ],
    marcasController.actualizar
);
// Eliminar Marcas
// api/marcas/:id
router.delete('/marcas/:id',
    auth,
    marcasController.eliminar
);
// Listar marcas
// api/marcas/
router.get('/marcas/',
    
    marcasController.listar
);

//creo los productos
// api/productos/
router.post('/productos/',
    auth,
    [
        check('estadoCel','El Estado del Producto es Obligatorio.').notEmpty(),
        check('marcaid','La Marca es obligatoria.').notEmpty(),
        check('marcaid','La Marca no es válida.').isMongoId(),
        check('modelo','El Modelo del Producto es Obligatorio.').notEmpty(),
        check('precio','El precio es obligatorio.').notEmpty(),
        check('precio','Ingrese un precio válido.').isFloat({min: 1}),
        check('caracteristicas','El caracteristicas es obligatorio.').notEmpty(),
        check('imgCel','El imagen es obligatorio.').notEmpty(),
    ],
    productoController.crear
);
// api/productos/
router.put('/productos/:id',
    auth,
    [
        check('estadoCel','El Estado del Producto es Obligatorio.').notEmpty(),
        check('marcaid','La Marca es obligatoria.').notEmpty(),
        check('marcaid','La Marca no es válida.').isMongoId(),
        check('modelo','El Modelo del Producto es Obligatorio.').notEmpty(),
        check('precio','El precio es obligatorio.').notEmpty(),
        check('precio','Ingrese un precio válido.').isFloat({min: 1}),
        check('caracteristicas','El caracteristicas es obligatorio.').notEmpty(),
        check('imgCel','El imagen es obligatorio.').notEmpty(),
    ],
    productoController.actualizar
);
// Eliminar Poductos
// api/productos/:id
router.delete('/productos/:id',
    auth,
    productoController.eliminar
);
// Listar productos
// api/productos/
router.get('/productos/',
    // auth,
    productoController.listar
);
// Listar productos menor Precio
// api/productos/
router.get('/productos/menorPrecio',
    productoController.listarMenorPrecio
);
// Listar productos mayor Precio
// api/productos/
router.get('/productos/mayorPrecio',
    productoController.listarMayorPrecio
);
// Listar productos por Marca
// api/productos/
router.get('/productos/porMarca/:marcaid',
    productoController.listarProductoPorMarca
);

module.exports = router;