// Rutas para auth usuarios
const express = require("express");
const router = express.Router();
const { check } = require("express-validator");
const authController = require('../controllers/authController')
const auth = require('../middleware/auth');

//inciar sesion
router.post('/auth', 
[
    check('email', 'Agrega un email valido').isEmail(),
    check('password', 'Paswword minimo 6 caracteres').isLength({min: 6})
],
 authController.autenticarUsurio);

//Obtener usuario autenticado
router.get('/auth',
    auth,
    authController.usuarioAutenticado
)

module.exports = router;
