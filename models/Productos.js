const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

// Definir Schema
const ProductoSchema = new Schema({
    estadoCel : {
        type: String,
        required: true,
        trim: true
    },
    marcaid: {
        type: Schema.Types.ObjectId,
        ref: 'Marcas',
        required: true,
    },
    modelo : {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    precio: {
        type: Number,
        required: true
    },
    disponible: {
        type: Boolean,
        default: false,
    },
    caracteristicas : {
        type: String,
        required: true,
        trim: true
    },
    imgCel: {
        type: String,
        required: true,
        trim: true
    },
    cantidad: {
        type: Number,
        default: 0
    },
    stock: {
        type: Number,
        default: 0
    },
    reservado: {
        type: Number,
        default: 0
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    }
});
//esto es para update_at
ProductoSchema.pre('save', async function (){
    console.log(this);
    this.updated_at = new Date();
});
//pagination
ProductoSchema.plugin(mongoosePaginate);
// Definimos el modelo con el Schema
module.exports = mongoose.model('Producto', ProductoSchema);