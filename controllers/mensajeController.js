const Mensaje = require("../models/Mensaje");
const { validationResult } = require("express-validator");

exports.crearMensajes = async (req, res) => {

  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() })
  }
  try {

    mensaje = new Mensaje(req.body);

    //guardar mensaje
    await mensaje.save();
    res.json({ msg: "Mensaje creado correctamente " + mensaje });


  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: "Hubo un error al crear el mensaje" });
  }
};

//obtener mensajes
exports.obtenerMensajes = async (req, res) => {

  try {
    await Mensaje.find().then(mensaje => res.send(mensaje))

  } catch (error) {
    console.log(error);
    res.status(500).send("hubo un error")
  }
}

// Actualizar a mensaje leido
exports.actualizar = async (req, res) => {

  // Validación de campos
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  try {
    
    // Verificar que mensaje exista
    let mensaje = await Mensaje.findById(req.params.id);

    if (!mensaje) {
      return res.status(404).json({ msg: 'El Proveedor no existe.' });
    }

    let update = await Mensaje.findById(req.params.id);
    update.is_leido = true
    console.log(update);

    await update.save();

    res.json({ msg: 'Marcado como leido', update });
  } catch (error) {
    console.log(error);
    res.status(400).json({ msg: 'Hubo un error.' });
  }
}
