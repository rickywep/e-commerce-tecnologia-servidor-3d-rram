const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const proveedorController = require('../controllers/proveedorController');
const compraproductosController = require('../controllers/compraproductosController');
const auth = require('../middleware/auth');

//creo el proveedor
// api/proveedor/
router.post('/proveedor/',
    auth,
    [
        check('nombre','El nombre del proveedor es Obligatorio.').notEmpty(),
        check('email', 'Agrega un email valido').notEmpty(),
        check('email', 'Agrega un email valido').isEmail(),
        check('celular', 'Agrega un email valido').notEmpty(),
        check('celular', 'Agrega un celular valido').isNumeric(),
    ],
    proveedorController.crear
);
//modifico el proveedor
// api/proveedor/:id
router.put('/proveedor/:id',
    auth,
    [
        check('nombre','El nombre del proveedor es Obligatorio.').notEmpty(),
        check('email', 'Agrega un email valido').notEmpty(),
        check('email', 'Agrega un email valido').isEmail(),
        check('celular', 'Agrega un email valido').notEmpty(),
        check('celular', 'Agrega un celular valido').isNumeric(),
    ],
    proveedorController.actualizar
);
// Eliminar un Proveedor
// api/proveedor/:id
router.delete('/proveedor/:id',
    auth,
    proveedorController.eliminar
);
// Listar Proveedores
// api/proveedor/
router.get('/proveedor/',
    auth,
    proveedorController.listar
);

//creo la compra
// api/compra/
router.post('/compra/',
    auth,
    [
        check('proveedorid','El ingreso del Proveedor es obligatorio.').notEmpty(),
        check('proveedorid','El Proveedor no es válido.').isMongoId(),
        check('productoid','El ingreso del Producto es obligatorio.').notEmpty(),
        check('productoid','El Producto no es válido.').isMongoId(),
        check('cantidad','La cantidad es obligatoria.').notEmpty(),
        check('preciocompra','El precio de compra es obligatorio.').notEmpty(),
        check('preciocompra','Ingrese un precio de compra válido.').isFloat({min: 1}),
        check('cambiaprecio','Debe ingresar si cambia o no de precio de ventas.').notEmpty(),
        check('precioventa','El precio de venta es obligatorio.').notEmpty(),
        check('precioventa','Ingrese un precio de venta válido.').isFloat({min: 1}),
        check('precioanterior','El precio de venta anterior es obligatorio.').notEmpty(),
        check('precioanterior','Ingrese un precio anterior de venta válido.').isFloat({min: 1}),
    ],
    compraproductosController.crear
);
// Eliminar un compra y deshacer el stock y precio
// api/compra/:id
router.delete('/compra/:id',
    auth,
    compraproductosController.eliminar
);
// Listar todas las compras
// api/compra/
router.get('/compra/',
    auth,
    compraproductosController.listar
);
// Listar las compras por proveedor y fechas
// api/compralist/
router.get('/compralist/',
    auth,
    [
        check('diainicial','Fecha inicial es Obligatoria.').notEmpty(),
        check('diafinal', 'Fecha final es Obligatoria.').notEmpty(),
    ],
    compraproductosController.listarProvFechas
);

module.exports = router;