// Rutas para usuarios
const express = require("express");
const router = express.Router();
const usuarioController = require("../controllers/usuarioController");
const { check } = require("express-validator");
const auth = require('../middleware/auth');

//crear usuarios
//api/usuarios
router.post("/usuarios", [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'Agrega un email valido').isEmail(),
    check('password', 'Paswword minimo 6 caracteres').isLength({min: 6})
], usuarioController.crearUsuarios);
//obtener usuarios
router.get('/usuarios',
    auth,
    usuarioController.obtenerUsuarios
)


module.exports = router;
