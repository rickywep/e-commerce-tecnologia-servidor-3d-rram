// Rutas para mensajes
const express = require("express");
const router = express.Router();
const mensajeController = require("../controllers/mensajeController");
const { check } = require("express-validator");
const auth = require('../middleware/auth');

//crear mensajes
//api/mensajes
router.post("/mensajes", [
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('apellido', 'El apellido es obligatorio').not().isEmpty(),
    check('celular', 'Celular es obligatorio').not().isEmpty(),
    check('email', 'Agrega un email valido').isEmail(),
    check('contenido', 'Escriba un mensaje por favor').not().isEmpty(),
], mensajeController.crearMensajes);
//obtener mensajes
router.get('/mensajes',
    auth,
    mensajeController.obtenerMensajes
)
router.put('/mensajes/:id',
    auth,
    mensajeController.actualizar
);


module.exports = router;
