const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require('jsonwebtoken')

exports.crearUsuarios = async (req, res) => {

  const errores = validationResult(req);
  if (!errores.isEmpty()) {
      return res.status(400).json({errores: errores.array()})
  } 
    
  const { email, password } = req.body;

  try {
    let usuario = await Usuario.findOne({ email });

    if (usuario) {
      return res.status(400).json({ msg: "El email ya esta registrado" });
    }

    usuario = new Usuario(req.body);

    //has password
    const salt = await bcryptjs.genSalt(10);
    usuario.password = await bcryptjs.hash(password, salt);

    //guardar usuario
    await usuario.save();

    //crear y firmar jwt
    const payload = {
        usuario:{
            id: usuario.id
        }

    };
    jwt.sign(payload, process.env.SECRETA,{
        expiresIn: 3600 //1 hora
    }, (error,token) => {
        if (error) throw error;
        res.json({token})
    })

  } catch (error) {
    console.log("error");
    res.status(400).send("Hubo un error");
  }
};

//obtener usuarios
exports.obtenerUsuarios = async (req,res) =>{

  try {
     await Usuario.find().select('nombre email is_admin').then(usuarios => res.send(usuarios))
    
  } catch (error) {
    console.log(error);
    res.status(500).send("hubo un error")    
  }
}