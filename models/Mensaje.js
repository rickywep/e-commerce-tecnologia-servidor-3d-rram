const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MensajeSchema = Schema({
  nombre: {
    type: String,
    require: true,
    trim: true,
  },
  apellido: {
    type: String,
    require: true,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    trim: true,
  },
  celular: {
    type: Number,
  },
  contenido: {
    type: String,
    require: true,
    trim: true,
  },
  is_leido: {
    type: Boolean,
    default: false,
  },
  create_at: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Mensaje", MensajeSchema);
